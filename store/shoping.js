export const state = () => ({
  shopingCart: []
})

export const mutations = {
  addProductIntoCart(state, payload) {
    const isAlreadyInTheCart = state.shopingCart.filter(
      (e) => e.id === payload.id
    )
    if (isAlreadyInTheCart.length > 0) {
      state.shopingCart.forEach((element) => {
        if (element.id === payload.id) {
          element.amount++
        }
      })
    } else {
      state.shopingCart.push({ ...payload, amount: 1 })
    }
  },
  removeProductFromCart(state, payload) {
    const remaining = state.shopingCart.filter(
      (e) => e.prodCode !== payload.prodCode
    )
    state.shopingCart = remaining
  },
  clearShopingCart(state) {
    state.shopingCart = []
  }
}
